.PHONY: run run2

run:
	python3 tzughi.py 2>&1 | sed 's/^  File "\([^"]*\)", line \([0-9]*\),/\1:\2:/'

run2:
	python2 tzughi.py
