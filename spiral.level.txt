[map]
#                   1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2
# 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9
.@. . . . . . . .
                .
. . . . . . .   .
.           .   .
.   . . .   .   .
.   .   x   .   .
.   .       .   .
.   . . . . .   .
.               .
. . . . . . . . .
[legend]
. floor type=normal
* wall
@ hero
x exit
a key color=yellow
A door we key=yellow
$ money value=5
% money value=20
[ui]
width 10
height 11
lpanel 2
fps 30
move_ticks 6
turn_ticks 3
# vim: set ft=text nowrap :
